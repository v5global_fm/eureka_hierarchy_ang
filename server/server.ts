var express = require('express');
var cors = require('cors')
let app = express();
const path = require('path');
app.use(cors());
var bodyParser = require('body-parser');
app.use(bodyParser.json());

require('dotenv/config');

app.use('/', express.static(path.join(__dirname, '/public'), { etag: false }));

//Server Listening

const port: any = process.env.PORT || '8079';
var server = app.listen(port, function () {
    console.log(`Server Listening on ${port}`);
});

// app.get('/', async function (req, res) {
//     res.send(res, "API's is live..", "");
// });

//Routes
var eureka = require('./routes/EurekaAPI');

app.use('/eureka', eureka);




