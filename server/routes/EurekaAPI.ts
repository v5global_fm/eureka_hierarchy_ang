
const Ssend = require('../Models/response')
const SqlCon = require('../auth/connection');
//const authorization = require('../auth/authorization');

import { Request, Response, NextFunction } from 'express'

const Router = require('express-promise-router')();

Router.route('/').get(async function (req, res) {
    Ssend.send(res, "API's is live..", "");
});

Router.route('/GetZones').get(async function (req: Request, res: Response, next: NextFunction) {

    try {
        const { recordset } = await SqlCon.query(`Select Distinct Zone From Stores Where BrandId=32 
                                                    AND Zone not like '%test%' Order By Zone`);
        Ssend.send(res, "Data Load Successfully...", recordset);
        // recordset.push(req.user)
        // res.send(recordset)

    }
    catch (err) {
        Ssend.error(res, 'Error occured..', err);
    }
});


Router.route('/GetRegionsByZone').post(async function (req: Request, res: Response, next: NextFunction) {

    try {

        //console.log(req.body);
        let { Zones } = req.body;
        //console.log(`'${Zones}'`);
        let { recordset } = await SqlCon.query(`Select Distinct Region from 
                                GETActivePromoterByZoneRegionChannel(GETDATE(),'All','All','All') GAP
                                LEFT JOIN SubChannelDetails SCD ON GAP.SubChannelType=SCD.SubChannelType
                                Where 
                                Zone IN (Select * from GetTableFromString('${Zones}')) 
                                ORDER BY Region`);
        //console.log(recordset)
        Ssend.send(res, 'Data Load Succcessfully..', recordset);
    }
    catch (err) {
        Ssend.error(res, 'Error occured', err);
    }
});

Router.route('/GetChannelByZoneRegion').post(async function (req: Request, res: Response, next: NextFunction) {
    try {
        let { Zones, Regions } = req.body;
        const { recordset } = await SqlCon.query(`Select Distinct SCD.ChannelCode AS Channel From 
                                    GETActivePromoterByZoneRegionChannel(GETDATE(),'All','All','All') GAP
                                    LEFT JOIN SubChannelDetails SCD ON GAP.SubChannelType=SCD.SubChannelType
                                    Where 
                                    Zone IN (Select * from GetTableFromString('${Zones}')) AND
                                    Region IN (Select * from GetTableFromString('${Regions}'))
                                    Order By SCD.ChannelCode`);
        Ssend.send(res, 'Data Load Successfully.', recordset);

    } catch (err) {
        Ssend.error(res, 'Error occured.', err);
    }
})

Router.route('/GetStatesByZoneRegionChannel').post(async function (req: Request, res: Response, next: NextFunction) {

    try {

        let { Zones, Regions, Channels } = req.body;
        //console.log(req.body);
        const { recordset } = await SqlCon.query(`Select Distinct State From 
                                    GETActivePromoterByZoneRegionChannel(GETDATE(),'All','All','All') GAP
                                    LEFT JOIN SubChannelDetails SCD ON GAP.SubChannelType=SCD.SubChannelType
                                    Where 
                                    Zone IN (Select * from GetTableFromString('${Zones}')) AND
                                    Region IN (Select * from GetTableFromString('${Regions}')) AND
                                    SCD.ChannelCode IN (Select * from GetTableFromString('${Channels}'))
                                    Order By State`);
        Ssend.send(res, 'Data Load Successfully.', recordset);

    } catch (err) {
        Ssend.error(res, 'Error occured.', err);
    }
});

Router.route('/GetCityByZoneRegionChannelState').post(async function (req: Request, res: Response, next: NextFunction) {

    try {
        let { Zones, Regions, Channels, States } = req.body;
        const { recordset } = await SqlCon.query(`SELECT Distinct GAP.City FROM 
                            GETActivePromoterByZoneRegionChannel(GETDATE(),'All','All','All') GAP
                            LEFT JOIN SubChannelDetails SCD ON GAP.SubChannelType=SCD.SubChannelType
                            Where 
                            Zone IN (Select * from GetTableFromString('${Zones}')) AND
                            Region IN (Select * from GetTableFromString('${Regions}')) AND
                            SCD.ChannelCode IN (Select * from GetTableFromString('${Channels}')) AND
                            State IN (Select * from GetTableFromString('${States}'))
                             Order By City`);

        Ssend.send(res, 'Data Load Successfully..', recordset);

    } catch (err) {
        Ssend.error(res, 'Error occured.', err);
    }
})

Router.route('/GetTerritoryByZoneRegionChannelStateCity').post(async function (req: Request, res: Response, next: NextFunction) {

    try {
        let { Zones, Regions, Channels, States, City } = req.body;
        const { recordset } = await SqlCon.query(`SELECT Distinct GAP.Territory FROM 
                            GETActivePromoterByZoneRegionChannel(GETDATE(),'All','All','All') GAP
                            LEFT JOIN SubChannelDetails SCD ON GAP.SubChannelType=SCD.SubChannelType
                            Where 
                            Zone IN (Select * from GetTableFromString('${Zones}')) AND
                            Region IN (Select * from GetTableFromString('${Regions}')) AND
                            SCD.ChannelCode IN (Select * from GetTableFromString('${Channels}')) AND
                            State IN (Select * from GetTableFromString('${States}')) AND
                            City IN (Select * from GetTableFromString('${City}'))
                            Order By Territory`);

        Ssend.send(res, 'Data Load Successfully..', recordset);

    } catch (err) {
        Ssend.error(res, 'Error occured.', err);
    }
})

Router.route('/GetBranchByZoneRegionChannelStateCityTerritory').post(async function (req: Request, res: Response, next: NextFunction) {

    try {
        let { Zones, Regions, Channels, States, City, Territory } = req.body;
        const { recordset } = await SqlCon.query(`SELECT Distinct GAP.BranchCode FROM 
                            GETActivePromoterByZoneRegionChannel(GETDATE(),'All','All','All') GAP
                            LEFT JOIN SubChannelDetails SCD ON GAP.SubChannelType=SCD.SubChannelType
                            Where 
                            Zone IN (Select * from GetTableFromString('${Zones}')) AND
                            Region IN (Select * from GetTableFromString('${Regions}')) AND
                            SCD.ChannelCode IN (Select * from GetTableFromString('${Channels}')) AND
                            State IN (Select * from GetTableFromString('${States}')) AND
                            City IN (Select * from GetTableFromString('${City}')) AND
                            Territory IN (Select * from GetTableFromString('${Territory}'))
                             Order By BranchCode`);

        Ssend.send(res, 'Data Load Successfully..', recordset);

    } catch (err) {
        Ssend.error(res, 'Error occured.', err);
    }
})

Router.route('/GetStoreByZoneRegionChannelStateCityTerritoryBranch').post(async function (req: Request, res: Response, next: NextFunction) {

    try {
        //console.log(req.body);
        let { Zones, Regions, Channels, States, City, Territory, BranchCode } = req.body;
        const { recordset } = await SqlCon.query(`SELECT Distinct GAP.StoreID,GAP.StoreName FROM 
                            GETActivePromoterByZoneRegionChannel(GETDATE(),'All','All','All') GAP
                            LEFT JOIN SubChannelDetails SCD ON GAP.SubChannelType=SCD.SubChannelType
                            Where 
                            Zone IN (Select * from GetTableFromString('${Zones}')) AND
                            Region IN (Select * from GetTableFromString('${Regions}')) AND
                            SCD.ChannelCode IN (Select * from GetTableFromString('${Channels}')) AND
                            State IN (Select * from GetTableFromString('${States}')) AND
                            City IN (Select * from GetTableFromString('${City}')) AND
                            Territory IN (Select * from GetTableFromString('${Territory}')) AND
                            BranchCode IN (Select * from GetTableFromString('${BranchCode}'))
                            Order By GAP.StoreID`);


        Ssend.send(res, 'Data Load Successfully..', recordset);

    } catch (err) {
        Ssend.error(res, 'Error occured.', err);
    }
})

Router.route('/SaveUserRights').post(async function (req: Request, res: Response, next: NextFunction) {

    try {
        //console.log(req.body);
        let { CreatedBy, Zones, Regions, Channels, States, City, Territory, BranchCode, Stores, Username, Password, Designation, Active } = req.body;

        const userData = await SqlCon.query(`EXEC EU_SaveOtherUsers ${Username},${Password},${Designation}`);

        if (userData.recordset[0].MSG == `LoginID ${Username} Created Successfully..`) {

            const TblCol = [{ "Tabel": "EH_ZoneMapping", "Column": "ZoneName", "data": Zones },
            { "Tabel": "EH_RegionMapping", "Column": "RegionName", "data": Regions },
            { "Tabel": "EH_ChannelMapping", "Column": "ChannelName", "data": Channels },
            { "Tabel": "EH_StateMapping", "Column": "StateName", "data": States },
            { "Tabel": "EH_CityMapping", "Column": "CityName", "data": City },
            { "Tabel": "EH_TerritoryMapping", "Column": "TerritoryName", "data": Territory },
            { "Tabel": "EH_BranchCodeMapping", "Column": "BranchCode", "data": BranchCode },
            { "Tabel": "EH_StoreMapping", "Column": ["Storeid", "StoreName"], "data": Stores }
            ];

            let maxArr = [];
            for (var key in req.body) {
                if (req.body[key].length != undefined) {
                    maxArr.push(req.body[key].length);
                }
            }
            //console.log(maxArr)
            for (var t = 0; t < TblCol.length; t++) {

                for (var i = 0; i < Math.max(...maxArr); i++) {


                    //console.log(TblCol[t].data[i])
                    if (TblCol[t].data[i] != null) {

                        if (TblCol[t].Tabel == "EH_StoreMapping") {

                            const datasplit = TblCol[t].data[i].split('<')
                            const storeid = datasplit[1].split('>')
                            //console.log(storeid[0] + "--" + datasplit[0])
                            const storeMSG = await SqlCon.query(`DECLARE @Msg VARCHAR(100)=''

                        IF EXISTS(SELECT * FROM [dbo].[${TblCol[t].Tabel}] WHERE UserId=${userData.recordset[0].UserId} and ${TblCol[t].Column[0]}=${storeid[0]})
                            BEGIN
                                SET @Msg='Already Exists'
                            END
                        ELSE
                            BEGIN
                                    INSERT INTO [dbo].[${TblCol[t].Tabel}](UserId,${TblCol[t].Column[0]},${TblCol[t].Column[1]},CreatedBy,CreatedOn,IsActive)
                                    VALUES(${userData.recordset[0].UserId},${storeid[0]},'${datasplit[0]}','${CreatedBy}',GETDATE(),'${Active}')

                                SELECT @Msg='Created Stores Successfully..'
                            END

                            SELECT @Msg AS MSG`)
                            //console.log(storeMSG.recordset[0].MSG)
                        }
                        else {
                            const tableMSG = await SqlCon.query(`DECLARE @Msg VARCHAR(100)=''

                        IF EXISTS(SELECT * FROM [dbo].[${TblCol[t].Tabel}] WHERE UserId=${userData.recordset[0].UserId} and ${TblCol[t].Column}='${TblCol[t].data[i]}')
                            BEGIN
                                SET @Msg='Already Exists'
                            END
                        ELSE
                            BEGIN
                                    INSERT INTO [dbo].[${TblCol[t].Tabel}](UserId,${TblCol[t].Column},CreatedBy,CreatedOn,IsActive)
                                    VALUES(${userData.recordset[0].UserId},'${TblCol[t].data[i]}','${CreatedBy}',GETDATE(),'${Active}')

                                SELECT @Msg='Created Successfully..'
                            END

                            SELECT @Msg AS MSG`)
                            //console.log(tableMSG.recordset[0].MSG)
                        }
                    }
                }
            }
        }
        Ssend.send(res, userData.recordset[0].MSG, "");
    } catch (err) {
        Ssend.error(res, 'Error occured.', err);
    }
})

// Router.route('/CreateManualObject').post(async function (req, res) {

//     const { recordset } = await SqlCon.query(`Select * From 
//                         GetISPMTDLMTDLYTDSalesData_App('2020-01-01','2020-01-31','All','All','All')`)

//     const category = ["UV", "RO", "EWP", "LVC", "PVC", "VC", "Cartridge", "STG", "NEWP", "AP", "AC"]


//     var { recordsets } = await SqlCon.query(`Select * From 
//                                    GETActivePromoterByZoneRegionChannel('2020-01-01','All','All','All')`)

//     // const data = recordset.map(x => x.Zone).
//     //     filter((value, index, self) => self.indexOf(value) === index && !value.includes('test'))

//     let dataArr = [];
//     for (var i = 0; i < recordsets.length; i++) {

//         const CatG = [];
//         category.forEach(ele => {
//             if (ele == "EWP") {
//                 CatG.push({
//                     name: ele,
//                     SaleType:
//                     {
//                         FTD: recordset.filter(x => x.LoginID == recordsets[0][i].V5ID &&
//                             (x.Category == "RO" || x.Category == "UV"))
//                             .map(y => y.Value).reduce(function (a, b) { return a + b / 100000; }, 0).
//                             toFixed(2) + "L",
//                         FTDVol: recordset.filter(x => x.LoginID == recordsets[0][i].V5ID &&
//                             (x.Category == "RO" || x.Category == "UV"))
//                             .map(y => y.QTY).reduce(function (a, b) { return a + b; }, 0)
//                     }

//                 })
//             }
//             else if (ele == "VC") {
//                 CatG.push({
//                     name: ele,
//                     SaleType:
//                     {
//                         FTD: recordset.filter(x => x.LoginID == recordsets[0][i].V5ID &&
//                             (x.Category == "PVC" || x.Category == "LVC"))
//                             .map(y => y.Value).reduce(function (a, b) { return a + b / 100000; }, 0).
//                             toFixed(2) + "L",
//                         FTDVol: recordset.filter(x => x.LoginID == recordsets[0][i].V5ID &&
//                             (x.Category == "PVC" || x.Category == "LVC"))
//                             .map(y => y.QTY).reduce(function (a, b) { return a + b; }, 0)
//                     }

//                 })
//             }
//             else if (ele == "NEWP") {
//                 CatG.push({
//                     name: ele,
//                     SaleType:
//                     {
//                         FTD: recordset.filter(x => x.LoginID == recordsets[0][i].V5ID &&
//                             (x.Category == "Cartridge" || x.Category == "STG"))
//                             .map(y => y.Value).reduce(function (a, b) { return a + b / 100000; }, 0).
//                             toFixed(2) + "L",
//                         FTDVol: recordset.filter(x => x.LoginID == recordsets[0][i].V5ID &&
//                             (x.Category == "Cartridge" || x.Category == "STG"))
//                             .map(y => y.Value).reduce(function (a, b) { return a + b; }, 0)
//                     }

//                 })
//             }
//             else if (ele == "AC") {
//                 CatG.push({
//                     name: ele,
//                     SaleType:
//                     {
//                         FTD: recordset.filter(x => x.LoginID == recordsets[0][i].V5ID && x.Category == "SAC")
//                             .map(y => y.Value).reduce(function (a, b) { return a + b / 100000; }, 0).
//                             toFixed(2) + "L",
//                         FTDVol: recordset.filter(x => x.LoginID == recordsets[0][i].V5ID && x.Category == "SAC")
//                             .map(y => y.QTY).reduce(function (a, b) { return a + b; }, 0)
//                     }

//                 })
//             }
//             else {
//                 CatG.push({
//                     name: ele,
//                     SaleType:
//                     {
//                         FTD: recordset.filter(x => x.LoginID == recordsets[0][i].V5ID && x.Category == ele)
//                             .map(y => y.Value).reduce(function (a, b) { return a + b / 100000; }, 0).
//                             toFixed(2) + "L",
//                         FTDVol: recordset.filter(x => x.LoginID == recordsets[0][i].V5ID && x.Category == ele)
//                             .map(y => y.QTY).reduce(function (a, b) { return a + b; }, 0)
//                     }

//                 })

//             }
//         })


//         dataArr.push({
//             ISPName: recordsets[0][i].Name,
//             EmpCode: recordsets[0][i].V5ID,
//             ContactNo: recordsets[0][i].Mobile1,
//             StoreId: recordsets[0][i].StoreID,
//             StoreName: recordsets[0][i].StoreName,
//             StoreAddress: recordsets[0][i].address,
//             Category: CatG
//         })
//     }
//     res.send(dataArr);
// })

module.exports = Router;





