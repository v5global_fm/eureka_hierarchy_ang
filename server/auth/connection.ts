const MSSQL = require('mssql');
const util = require('util');
require('util.promisify').shim();

//Importing Environment Variables.
require('dotenv/config');

const { MSSQL_USERNAME, MSSQL_PASSWORD, MSSQL_HOST, MSSQL_DATABASE, MSSQL_PORT } = process.env;

const pool = new MSSQL.ConnectionPool({
    user: MSSQL_USERNAME,
    password: MSSQL_PASSWORD,
    server: MSSQL_HOST,
    database: MSSQL_DATABASE,
    requestTimeout: 300000

});

pool.connect(err => {
    console.log(err, 'CONNECT');
});

pool.query = util.promisify(pool.query);

module.exports = pool;