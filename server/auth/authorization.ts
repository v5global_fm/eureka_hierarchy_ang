const JWT = require('jsonwebtoken');

module.exports = {
    VerifyAuth: (req, res, next) => {
        const token = req.headers["authorization"]
        if (token == null) return res.sendStatus(401)

        JWT.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
            if (err) return res.sendStatus(403)

            req.user = user
            next()
        })
    },
    getTokens: (req, res, next) => {
        const username = req.username
        if (username == null) return res.sendStatus(403)

        const user = { name: username }
        req.authorization = JWT.sign(user, process.env.ACCESS_TOKEN_SECRET)
        next()
    }
}

