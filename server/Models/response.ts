
import { Response } from 'express'

module.exports = {
    send: (res: Response, message: string, data: any) => {
        return res.status(200).json({ status: true, message, data: data || {} });
    },
    error: (res: Response, message: string, error: any) => {
        const ErrObj: object = { status: false, message, error: error || {} };
        return res.status(ErrObj);
    }
};