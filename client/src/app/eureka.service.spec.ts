import { TestBed } from '@angular/core/testing';

import { EurekaService } from './eureka.service';

describe('EurekaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EurekaService = TestBed.get(EurekaService);
    expect(service).toBeTruthy();
  });
});
