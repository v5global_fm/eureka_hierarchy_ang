import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms'
import { EurekaService } from '../eureka.service';
import { MatSnackBar } from '@angular/material';
import { ThemePalette } from '@angular/material/core';
import { ProgressSpinnerMode } from '@angular/material/progress-spinner';



@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  designationArr = ["TL", "TIC", "BIC", "CH", "ZBH", "BH", "MO", "ME", "C&TM GR", "C&TM OR", "NTSE", "CnTM"];
  designations = new FormControl();
  checked = false;
  classProp = "loader-class";
  color: ThemePalette = 'primary';
  mode: ProgressSpinnerMode = 'indeterminate';
  value = 50;
  hide: boolean;

  constructor(private _eurekaSerivce: EurekaService, private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this._eurekaSerivce.getZoneData().subscribe((data: any) => {
      data.data.forEach(element => {
        //console.log(element.Zone)
        this.zoneList.push(element.Zone);
      });
      this.classProp = "Noloader";
    }, (err: any) => {
      this.classProp = "Noloader";
      this._snackBar.open("Server is not working.", 'X', { duration: 4000 });
      console.log(err);
    })

  }

  username = new FormControl();
  password = new FormControl();
  rg = false;
  zones = new FormControl();
  regions = new FormControl();
  channels = new FormControl();
  states = new FormControl();
  cities = new FormControl();
  territories = new FormControl();
  branchcodes = new FormControl();
  stores = new FormControl();

  zoneList = [];
  regionList = [];
  channelList = [];
  stateList = [];
  cityList = [];
  territoryList = [];
  branchcodeList = [];
  storeList = [];

  ///////////////All select Function Start//////////////////////////

  selectAllzones(checkAll, regionAll) {
    this.classProp = "loader-class";
    regionAll._selected = false;
    if (checkAll._selected) {
      this.zones.setValue(this.zoneList);
      checkAll._selected = true;
      this.RegionOnChange();
    }
    else if (checkAll._selected == false) {
      this.zones.setValue([]);
      this.RegionOnChange();
    }
    else {
      this.zones.setValue(this.zones.value);
      this.RegionOnChange();
    }
  }

  selectAllregions(checkAll, channelAll) {
    this.classProp = "loader-class";
    channelAll._selected = false;
    if (checkAll._selected) {
      this.regions.setValue(this.regionList);
      checkAll._selected = true;
      this.ChannelOnChange();
    }
    else if (checkAll._selected == false) {
      this.regions.setValue([]);
      this.ChannelOnChange();
    }
    else {
      this.regions.setValue(this.regions.value);
      this.ChannelOnChange();
    }
  }

  selectAllchannels(checkAll, stateAll) {
    this.classProp = "loader-class";
    stateAll._selected = false;
    if (checkAll._selected) {
      this.channels.setValue(this.channelList);
      checkAll._selected = true;
      this.StateOnChange();
    }
    else if (checkAll._selected == false) {
      this.channels.setValue([]);
      this.StateOnChange();
    }
    else {
      this.channels.setValue(this.channels.value);
      this.StateOnChange();
    }
  }

  selectAllstates(checkAll, cityAll) {
    this.classProp = "loader-class";
    cityAll._selected = false;
    if (checkAll._selected) {
      this.states.setValue(this.stateList);
      checkAll._selected = true;
      this.CityOnChange();
    }
    else if (checkAll._selected == false) {
      this.states.setValue([]);
      this.CityOnChange();
    }
    else {
      this.states.setValue(this.states.value);
      this.CityOnChange();
    }
  }

  selectAllcities(checkAll, territoryAll) {
    this.classProp = "loader-class";
    territoryAll._selected = false;
    if (checkAll._selected) {
      this.cities.setValue(this.cityList);
      checkAll._selected = true;
      this.TerritoryOnChange();
    }
    else if (checkAll._selected == false) {
      this.cities.setValue([]);
      this.TerritoryOnChange();
    }
    else {
      this.cities.setValue(this.cities.value);
      this.TerritoryOnChange();
    }
  }

  selectAllterritory(checkAll, branchCodeAll) {
    this.classProp = "loader-class";
    branchCodeAll._selected = false;
    if (checkAll._selected) {
      this.territories.setValue(this.territoryList);
      checkAll._selected = true;
      this.BranchCodeOnChange();
    }
    else if (checkAll._selected == false) {
      this.territories.setValue([]);
      this.BranchCodeOnChange();
    }
    else {
      this.territories.setValue(this.territories.value);
      this.BranchCodeOnChange();
    }
  }

  selectAllbranchcode(checkAll, storesAll) {
    this.classProp = "loader-class";
    storesAll._selected = false;
    if (checkAll._selected) {
      this.branchcodes.setValue(this.branchcodeList);
      checkAll._selected = true;
      this.StoresOnChange();
    }
    else if (checkAll._selected == false) {
      this.branchcodes.setValue([]);
      this.StoresOnChange();
    }
    else {
      this.branchcodes.setValue(this.branchcodes.value);
      this.StoresOnChange();
    }
  }

  selectAllstores(checkAll) {

    if (checkAll._selected) {
      this.stores.setValue(this.storeList);
      checkAll._selected = true;
    }
    else if (checkAll._selected == false) {
      this.stores.setValue([]);
      checkAll._selected = false;
    }
    else {
      this.stores.setValue(this.stores.value);
    }
  }
  ///////////////All Select Function Ends//////////////////////////

  //////// On Change Function Start//////////////////////////
  RegionOnChange() {
    const body = { Zones: this.zones.value, Regions: this.regions.value };
    this._eurekaSerivce.getRegionsByZone(body).subscribe(data => {
      //console.log(data);
      this.regionList = []; this.regions.setValue([])
      this.channelList = []; this.channels.setValue([])
      this.stateList = []; this.states.setValue([])
      this.cityList = []; this.cities.setValue([])
      this.territoryList = []; this.territories.setValue([])
      this.branchcodeList = []; this.branchcodes.setValue([])
      this.storeList = []; this.stores.setValue([])
      data.data.forEach(element => {
        this.regionList.push(element.Region);
      });
      this.classProp = "Noloader";
    }, (err: any) => {
      this.classProp = "Noloader";
      this._snackBar.open("Server is not working.", 'X', { duration: 4000 });
      console.log(err);
    })
  }

  ChannelOnChange() {
    const body = { Zones: this.zones.value, Regions: this.regions.value };
    this._eurekaSerivce.getChannelByZoneRegion(body).subscribe(data => {
      //console.log(data);
      this.channelList = []; this.channels.setValue([])
      this.stateList = []; this.states.setValue([])
      this.cityList = []; this.cities.setValue([])
      this.territoryList = []; this.territories.setValue([])
      this.branchcodeList = []; this.branchcodes.setValue([])
      this.storeList = []; this.stores.setValue([])
      data.data.forEach(element => {

        this.channelList.push(element.Channel);
      });
      this.classProp = "Noloader";
    }, (err: any) => {
      this.classProp = "Noloader";
      this._snackBar.open("Server is not working.", 'X', { duration: 4000 });
      console.log(err);
    })
  }

  StateOnChange() {
    const body = { Zones: this.zones.value, Regions: this.regions.value, Channels: this.channels.value };
    this._eurekaSerivce.getStatesByZoneRegionChannel(body).subscribe(data => {
      //console.log(data);
      this.stateList = []; this.states.setValue([])
      this.cityList = []; this.cities.setValue([])
      this.territoryList = []; this.territories.setValue([])
      this.branchcodeList = []; this.branchcodes.setValue([])
      this.storeList = []; this.stores.setValue([])
      data.data.forEach(element => {

        this.stateList.push(element.State);
      });
      this.classProp = "Noloader";
    }, (err: any) => {
      this.classProp = "Noloader";
      this._snackBar.open("Server is not working.", 'X', { duration: 4000 });
      console.log(err);
    })
  }

  CityOnChange() {
    const body = {
      Zones: this.zones.value, Regions: this.regions.value, Channels: this.channels.value,
      States: this.states.value
    };
    this._eurekaSerivce.getCityByZoneRegionChannelState(body).subscribe(data => {
      //console.log(data);
      this.cityList = []; this.cities.setValue([])
      this.territoryList = []; this.territories.setValue([])
      this.branchcodeList = []; this.branchcodes.setValue([])
      this.storeList = []; this.stores.setValue([])
      data.data.forEach(element => {
        this.cityList.push(element.City);
      });
      this.classProp = "Noloader";
    }, (err: any) => {
      this.classProp = "Noloader";
      this._snackBar.open("Server is not working.", 'X', { duration: 4000 });
      console.log(err);
    })
  }

  TerritoryOnChange() {
    const body = {
      Zones: this.zones.value, Regions: this.regions.value, Channels: this.channels.value,
      States: this.states.value, City: this.cities.value
    };
    this._eurekaSerivce.getTerritoryByZoneRegionChannelStateCity(body).subscribe(data => {
      //console.log(data);
      this.territoryList = []; this.territories.setValue([])
      this.branchcodeList = []; this.branchcodes.setValue([])
      this.storeList = []; this.stores.setValue([])
      data.data.forEach(element => {
        this.territoryList.push(element.Territory);
      });
      this.classProp = "Noloader";
    }, (err: any) => {
      this.classProp = "Noloader";
      this._snackBar.open("Server is not working.", 'X', { duration: 4000 });
      console.log(err);
    })
  }

  BranchCodeOnChange() {
    const body = {
      Zones: this.zones.value, Regions: this.regions.value, Channels: this.channels.value,
      States: this.states.value, City: this.cities.value, Territory: this.territories.value
    };
    this._eurekaSerivce.getBranchByZoneRegionChannelStateCityTerritory(body).subscribe(data => {
      //console.log(data);
      this.branchcodeList = []; this.branchcodes.setValue([])
      this.storeList = []; this.stores.setValue([])
      data.data.forEach(element => {
        this.branchcodeList.push(element.BranchCode);
      });
      this.classProp = "Noloader";
    }, (err: any) => {
      this.classProp = "Noloader";
      this._snackBar.open("Server is not working.", 'X', { duration: 4000 });
      console.log(err);
    })
  }

  StoresOnChange() {
    const body = {
      Zones: this.zones.value, Regions: this.regions.value, Channels: this.channels.value,
      States: this.states.value, City: this.cities.value, Territory: this.territories.value,
      BranchCode: this.branchcodes.value
    };
    this._eurekaSerivce.getStoreByZoneRegionChannelStateCityTerritoryBranch(body).subscribe(data => {
      //console.log(data);
      this.storeList = []; this.stores.setValue([])
      data.data.forEach(element => {
        this.storeList.push(element.StoreName + "<" + element.StoreID + ">");
      });
      this.classProp = "Noloader";
    }, (err: any) => {
      this.classProp = "Noloader";
      this._snackBar.open("Server is not working.", 'X', { duration: 4000 });
      console.log(err);
    })
  }

  //////// On Change Function Ends//////////////////////////

  SaveUserRights() {

    if (this.designations.value == null) { this._snackBar.open("Please Select a Designation", 'X', { duration: 4000 }); return; }
    if (this.username.value == null) { this._snackBar.open("Please Enter Username", 'X', { duration: 4000 }); return; }
    if (this.password.value == null) { this._snackBar.open("Please Enter Password", 'X', { duration: 4000 }); return; }
    if (this.zones.value == null || this.zones.value.length == 0) { this._snackBar.open("Please Select Zones", 'X', { duration: 4000 }); return; }
    if (this.regions.value == null || this.regions.value.length == 0) { this._snackBar.open("Please Select Regions", 'X', { duration: 4000 }); return; }
    if (this.channels.value == null || this.channels.value.length == 0) { this._snackBar.open("Please Select Channels", 'X', { duration: 4000 }); return; }
    if (this.states.value == null || this.states.value.length == 0) { this._snackBar.open("Please Select States", 'X', { duration: 4000 }); return; }
    if (this.cities.value == null || this.cities.value.length == 0) { this._snackBar.open("Please Select City", 'X', { duration: 4000 }); return; }
    if (this.territories.value == null || this.territories.value.length == 0) { this._snackBar.open("Please Select Territory", 'X', { duration: 4000 }); return; }
    if (this.branchcodes.value == null || this.branchcodes.value.length == 0) { this._snackBar.open("Please Select BranchCodes", 'X', { duration: 4000 }); return; }
    if (this.stores.value == null || this.stores.value.length == 0) { this._snackBar.open("Please Select Stores", 'X', { duration: 4000 }); return; }
    if (this.checked == false) { this._snackBar.open("Please checked Active", 'X', { duration: 4000 }); return; }

    const body = {
      Username: this.username.value, Password: this.password.value,
      Zones: this.zones.value, Regions: this.regions.value, Channels: this.channels.value,
      States: this.states.value, City: this.cities.value, Territory: this.territories.value,
      BranchCode: this.branchcodes.value, Stores: this.stores.value, Designation: this.designations.value,
      Active: this.checked, CreatedBy: "saurabh"
    };

    this.classProp = "loader-class";
    //console.log(body);

    this._eurekaSerivce.saveUserRightsData(body).subscribe(data => {
      //console.log(data);
      if (data.status) {
        this.classProp = "Noloader";
        this._snackBar.open(data.message, 'X', { duration: 3000 });
      }
      else {
        this._snackBar.open(data.message, 'X', { duration: 3000 });
      }
    }, (err: any) => {
      console.log(err);
    })
  }

}
