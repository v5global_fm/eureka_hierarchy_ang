import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const baseUrl = environment.uri;

@Injectable({
  providedIn: 'root'
})

export class EurekaService {
  constructor(private http: HttpClient) { }
  getZoneData() {

    return this.http.get(`${baseUrl}/eureka/GetZones`);

  }
  getRegionsByZone(body: any) {
    return this.http.post<any>(`${baseUrl}/eureka/GetRegionsByZone`, body);
  }
  getChannelByZoneRegion(body: any) {
    return this.http.post<any>(`${baseUrl}/eureka/GetChannelByZoneRegion`, body);
  }
  getStatesByZoneRegionChannel(body: any) {
    return this.http.post<any>(`${baseUrl}/eureka/GetStatesByZoneRegionChannel`, body);
  }
  getCityByZoneRegionChannelState(body: any) {
    return this.http.post<any>(`${baseUrl}/eureka/GetCityByZoneRegionChannelState`, body);
  }
  getTerritoryByZoneRegionChannelStateCity(body: any) {
    return this.http.post<any>(`${baseUrl}/eureka/GetTerritoryByZoneRegionChannelStateCity`, body);
  }

  getBranchByZoneRegionChannelStateCityTerritory(body: any) {
    return this.http.post<any>(`${baseUrl}/eureka/GetBranchByZoneRegionChannelStateCityTerritory`, body);
  }

  getStoreByZoneRegionChannelStateCityTerritoryBranch(body: any) {
    return this.http.post<any>(`${baseUrl}/eureka/GetStoreByZoneRegionChannelStateCityTerritoryBranch`, body);
  }

  saveUserRightsData(body: any) {
    return this.http.post<any>(`${baseUrl}/eureka/SaveUserRights`, body);
  }

}
